#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
tests for Time Computer

MIT License
mrpenner
"""

import datetime
from decimal import Decimal

tc = __import__("tc")


def test_parse_line():
    assert tc.parse_line("20210331 jobname 06:47 11:59 12:52 17:03") == {
        "date": datetime.date(2021, 3, 31),
        "job": "jobname",
        "hours": 9.383333333333333,
        "miles": 0,
    }


def test_parse_line_with_mileage():
    assert tc.parse_line("20210331 jobname 06:47 11:59 12:52 17:03 mi 59.5") == {
        "date": datetime.date(2021, 3, 31),
        "job": "jobname",
        "hours": 9.383333333333333,
        "miles": 59.5,
    }


def test_daily_hours():
    assert tc.daily_hours(
        [
            {"date": datetime.date(2021, 3, 31), "job": "jobname", "hours": 9.38},
            {"date": datetime.date(2021, 3, 31), "job": "jobname", "hours": 7},
        ]
    ) == {datetime.date(2021, 3, 31): 16.380000000000003}


def test_weekly_hours():
    assert tc.weekly_hours(
        [
            {"date": datetime.date(2021, 3, 31), "job": "jobname", "hours": 9.38},
            {"date": datetime.date(2021, 3, 31), "job": "jobname", "hours": 7},
        ]
    ) == {13: 16.380000000000003}


def test_reg_overtime_hours():
    assert tc.reg_overtime_hours({1: 39, 2: 40, 3: 44.5, 4: 82}, 40) == {
        "regular": 159,
        "overtime": 46.5,
    }


def test_job_hours():
    assert tc.job_hours(
        [
            {"date": datetime.date(2021, 3, 31), "job": "jobname", "hours": 9.38},
            {"date": datetime.date(2021, 3, 31), "job": "jobname", "hours": 7},
            {"date": datetime.date(2021, 3, 31), "job": "jobname2", "hours": 9.38},
        ]
    ) == {"jobname": 16.380000000000003, "jobname2": 9.38}


def test_total_hours():
    assert (
        tc.total_hours(
            [
                {"date": datetime.date(2021, 3, 31), "job": "jobname", "hours": 9.38},
                {"date": datetime.date(2021, 3, 31), "job": "jobname", "hours": 7},
            ]
        )
        == 16.380000000000003
    )


def test_gross_pay():
    assert tc.gross_pay(
        Decimal("40"), Decimal("5.5"), Decimal("12.5"), Decimal("1.5")
    ) == {"gross pay": Decimal("603.13"), "effective rate": Decimal("13.26")}
