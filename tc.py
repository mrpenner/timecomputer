#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Time Computer version: 0.1.1
#
#  MIT License
#  mrpenner

import sys
import datetime as dt
from decimal import Decimal, ROUND_HALF_UP

settings = {
    "standard_week": Decimal(40),
    "rate": Decimal(0),
    "ot_mult": Decimal(1.5),
}


def parse_line(line):
    """
    parse a line (as a string) from the input file
    return the date, job name, hours, and miles in a dictionary
    """
    items = line.split()
    day = dt.date.fromisoformat(items[0])
    name = items[1]
    del items[0:2]

    times = []
    miles = 0
    for i, item in enumerate(items):
        # miles can be put at the end of a line using syntax "mi 59.5" for 59.5 miles
        if item == "mi":
            miles = float(items[i + 1])
            break
        clock = dt.datetime.combine(day, dt.time.fromisoformat(item))
        times.append(clock)
    accumulated_time = dt.timedelta()
    for i in range(1, len(times), 2):
        accumulated_time += times[i] - times[i - 1]
    hours = accumulated_time / dt.timedelta(hours=1)
    return {
        "date": day,
        "job": name,
        "hours": hours,
        "miles": miles,
    }


def daily_hours(data):
    """
    calculate how many hours worked each day
    return a dictionary with dates as keys and hours as values
    """
    output = {}
    for day in data:
        if day["date"] in output:
            output[day["date"]] += day["hours"]
        else:
            output[day["date"]] = day["hours"]
    return output


def weekly_hours(data):
    """
    calculate how many hours worked each week
    return a dictionary with ISO week numbers as keys and hours as values
    """
    output = {}
    for day in data:
        week = day["date"].isocalendar()[1]
        if week in output:
            output[week] += day["hours"]
        else:
            output[week] = day["hours"]
    return output


def reg_overtime_hours(weekly, standard_week):
    """
    calculate how much regular time and how much overtime worked
    take a dictionary from weekly_hours of ISO week numbers as keys and hours as values
    and standard_week an integer of hours when overtime begins
    return a dictionary like {"regular": 40, "overtime": 4.5}
    """
    # only needed until everything is converted to Decimals
    standard_week = float(standard_week)
    output = {"regular": 0, "overtime": 0}
    for week in weekly:
        if weekly[week] <= standard_week:
            output["regular"] += weekly[week]
        else:
            output["regular"] += standard_week
            output["overtime"] += weekly[week] - standard_week
    return output


def job_hours(data):
    """
    calculate how many hours worked each job
    return a dictionary with job names as keys and hours as values
    """
    output = {}
    for day in data:
        if day["job"] in output:
            output[day["job"]] += day["hours"]
        else:
            output[day["job"]] = day["hours"]
    return output


def total_hours(data):
    """
    calculate total hours worked
    return total hours as a float
    """
    total = 0.0
    for item in data:
        total += item["hours"]
    return total


def gross_pay(reg_hrs, ot_hrs, rate, ot_mult):
    """
    figure total gross pay and effective hourly rate
    """
    output = {}
    reg_hrs = Decimal(reg_hrs)
    ot_hrs = Decimal(ot_hrs)
    output["gross pay"] = Decimal(
        (reg_hrs * rate) + (ot_hrs * rate * ot_mult)
    ).quantize(Decimal(".01"), rounding=ROUND_HALF_UP)
    output["effective rate"] = Decimal(
        output["gross pay"] / (reg_hrs + ot_hrs)
    ).quantize(Decimal(".01"), rounding=ROUND_HALF_UP)
    return output


def time_computer(data, settings):
    """
    take dictionary of data from each line
    calculate and print all the results
    """
    print("\nhours by day")
    for day, hours in daily_hours(data).items():
        print(str(day) + ":", round(hours, 2))
    print("\nhours by week")
    weekly = weekly_hours(data)
    for week, hours in weekly.items():
        print(str(week) + ":", round(hours, 2))
    print("\nhours by job")
    for job_name, hours in job_hours(data).items():
        print(job_name + ":", round(hours, 2))
    print("")
    reg_ot = reg_overtime_hours(weekly, settings["standard_week"])
    for name, hours in reg_ot.items():
        print(name + ":", round(hours, 2))
    print("\ntotal hours:", round(total_hours(data), 2))
    print("total mileage:", round(sum([x["miles"] for x in data]), 2), "\n")
    if settings["rate"] > 0:
        for name, amount in gross_pay(
            reg_ot["regular"], reg_ot["overtime"], settings["rate"], settings["ot_mult"]
        ).items():
            print(name + ": $", amount)


if __name__ == "__main__":
    print("time computer")
    print("version 0.1.2")
    print("        ____")
    print("       /    \\")
    print("      /  |   \\")
    print("     |   |    |")
    print("      \\   \\  /")
    print("       \\____/")

    try:
        file_name = sys.argv[1]
    except IndexError:
        file_name = input("File Name: ")

    with open(file_name) as infile:
        data = [parse_line(line) for line in infile]

    time_computer(data, settings)
